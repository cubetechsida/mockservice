package com.mock.sb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.viettel.cm.register.ws.CheckSubExistsActive;
import com.viettel.cm.register.ws.CheckSubExistsActiveResponse;
import com.viettel.cm.register.ws.Return;

@Endpoint
public class SBEndpoint {
	private static final String NAMESPACE_URI = "http://ws.register.cm.viettel.com";


	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "checkSubExistsActive")
	@ResponsePayload
	public CheckSubExistsActiveResponse getCountry(@RequestPayload CheckSubExistsActive checkSubExistsActive) {
		String isdn = checkSubExistsActive.getWsRequest().getIsdn();
		List<String> isdnList = Arrays.asList(isdn.split(","));
		List<String> result = new ArrayList<String>();
		for (String s: isdnList) {
			if (!s.contains("696969")) {
				result.add(s);
			}
		}
		CheckSubExistsActiveResponse response = new CheckSubExistsActiveResponse();
		Return ret = new Return();
		if (result.isEmpty()) {
			ret.setCode(0);
			ret.setMessage("error!");
		} else {
			ret.setCode(1);
			String temp = result.toString().replaceAll(" ", "");
			ret.setReturnValue(temp.substring(1, temp.length() - 1));
		}
		response.setReturn(ret);
		return response;
	}
}
